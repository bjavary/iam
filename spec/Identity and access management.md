# Gaia-X Identity and Access Management specification
[TOC]
TEST
## Introduction

### Motivation

Considering the official purpose description of Gaia-X project :

> Gaia-X is a project initiated by Europe for Europe and beyond. Representatives from business, politics, and science from Europe and around the globe are working together, hand in hand, to create a federated and secure data infrastructure. Companies and citizens will collate and share data – in such a way that they keep control over them. They should decide what happens to their data, where it is stored, and always retain data sovereignty. 
>
> The architecture of Gaia-X is based on the principle of decentralisation. Gaia-X is the result of a multitude of individual platforms that all follow a common standard – the Gaia-X standard. Together, we are developing a data infrastructure based on the values of openness, transparency, and trust. So, what emerges is not a cloud, but a networked system that links many cloud services providers together.

One of the founding principles of the project is decentralization :

> The architecture of Gaia-X is based on the principle of decentralisation.

Our objective is :
* How parties authenticate themselves in a fully decentralized context (final target)
* How to adapt the ambition of decentralized identity management promulgated by Gaia-X to the current industrial reality (transition phase)

The fact is that to date, most companies (service providers) rely on the [OpenID Connect](https://openid.net/connect/) standard to authenticate their users.

The transition to a decentralized model ([SSI/DID](https://www.w3.org/TR/did-core/)) will certainly take several years and is not fully compatible with the schedule ambitions of the Gaia-X project.

The Gaia-X Technical Committee / Federation Services / Identity and Access Management working group aims at specifying mechanisms allowing the progressive use of decentralized authentication means while considering this industrial reality while keeping the fully decentralized mode as the final target.



### Definitions

| **Other Terms/Definitions** | **Description**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| resource                    | As defined by [[RFC3986](https://www.w3.org/TR/did-core/#bib-rfc3986)]: "...the term 'resource' is used in a general sense for whatever might be identified by a URI." Similarly, any resource might serve as a [DID subject](https://www.w3.org/TR/did-core/#dfn-did-subjects) identified by a [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers).                                                                                                                                                                                                                                                      |
| verifiable credential       | A standard data model and representation format for cryptographically-verifiable digital credentials as defined by the W3C Verifiable Credentials specification [[VC-DATA-MODEL](https://www.w3.org/TR/did-core/#bib-vc-data-model)].                                                                                                                                                                                                                                                                                                                                                                                      |
| DID document                | A set of data describing the [DID subject](https://www.w3.org/TR/did-core/#dfn-did-subjects), including mechanisms, such as cryptographic public keys, that the [DID subject](https://www.w3.org/TR/did-core/#dfn-did-subjects) or a [DID delegate](https://www.w3.org/TR/did-core/#dfn-did-delegate) can use to [authenticate](https://www.w3.org/TR/did-core/#dfn-authenticated) itself and prove its association with the [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers). A DID document might have one or more different [representations](https://www.w3.org/TR/did-core/#dfn-representations). |
| *To be completed*           | ...                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |




### Contributors

| Name             | Company   |
| ---------------- | --------- |
| Valerie Bruna    | Docapost  |
| Remi Blancher    | Atos      |
| Olivier Caudron  | OVH Cloud |
| Olivier Senot    | Docapost  |
| Bruno Javary     | Atos      |
| Bastien Vigneron | Outscale  |



## Centralized authentication principles



## Decentralized authentication principles



## Gaia-X context : Usage of decentralized identifiers in a centralized authentication world

### Identifiers format 
DID / SSI ...

### Identifiers storage

Wallets...

### DID usage in a OpenID context

How to use DID as OpenID credentials.

What are the acceptable rules and tradeoffs to reconcile these two worlds.

### Conformity
How to manage conformity considering the DID norm is not yet stabilized (eIDAS link, etc...)

## Gaia-X context : Fully decentralized authentication
How to use DID in a fully decentralized context ?

### Identifiers format 
DID / SSI ...

### Identifiers storage

Wallets...

### DID Doc storage 
What is the supported block chains ?

### Conformity
How to manage conformity considering the DID norm is not yet stabilized (eIDAS link, etc...)
